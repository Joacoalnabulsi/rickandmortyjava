package com.example.rickandmortyjava;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.example.rickandmortyjava.service.Character;
import com.example.rickandmortyjava.service.RickAndMortyService;
import java.util.List;

public class CharacterActivity extends AppCompatActivity implements Contract.View {
    private CharacterAdapter adapter;
    private Contract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CharacterAdapter();
        recyclerView.setAdapter(adapter);

        RickAndMortyService service = RetrofitApi.getService();
        presenter = new CharacterPresenter(this, service);
        presenter.getCharacters();
    }

    @Override
    public void showCharacters(List<Character> characterList) {
        adapter.setCharacters(characterList);
    }

    @Override
    public void showError(String message) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
