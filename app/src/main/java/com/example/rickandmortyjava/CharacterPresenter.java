package com.example.rickandmortyjava;

import androidx.annotation.NonNull;

import com.example.rickandmortyjava.service.RickAndMortyData;
import com.example.rickandmortyjava.service.RickAndMortyService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterPresenter implements Contract.Presenter {

    private final Contract.View view;
    private final RickAndMortyService service;

    public CharacterPresenter(Contract.View view, RickAndMortyService service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getCharacters() {
        service.getCharacterData().enqueue(new Callback<RickAndMortyData>() {
            @Override
            public void onResponse(@NonNull Call<RickAndMortyData> call, @NonNull Response<RickAndMortyData> response) {
                if (response.isSuccessful()) {
                    RickAndMortyData data = response.body();
                    if (data != null) {
                        view.showCharacters(data.getResults());
                    }
                } else {
                    view.showError("Failed to fetch characters.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<RickAndMortyData> call, @NonNull Throwable t) {
                view.showError("Network error: " + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroy() {

    }
}