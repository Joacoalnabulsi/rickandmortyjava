package com.example.rickandmortyjava;

import com.example.rickandmortyjava.service.RickAndMortyService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApi {
    private static final String BASE_URL = "https://rickandmortyapi.com/api/";

    private static Retrofit retrofit = null;

    public static RickAndMortyService getService() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(RickAndMortyService.class);
    }
}