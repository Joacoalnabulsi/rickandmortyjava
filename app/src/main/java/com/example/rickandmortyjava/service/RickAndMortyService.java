package com.example.rickandmortyjava.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RickAndMortyService {
    @GET("character")
    Call<RickAndMortyData> getCharacterData();
}