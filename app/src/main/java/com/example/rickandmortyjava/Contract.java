package com.example.rickandmortyjava;

import com.example.rickandmortyjava.service.Character;

import java.util.List;

public interface Contract {

    interface View {
        void showCharacters(List<Character> characterList);
        void showError(String message);
    }

    interface Presenter {
        void getCharacters();
        void onDestroy();
    }
}